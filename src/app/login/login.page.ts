import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth'
import { auth } from 'firebase/app'
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
 
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit{

    username: string = ""
    password: string = ""
    regMsg: string;

    constructor( public alertCtrl: AlertController, public afAuth: AngularFireAuth, public navCtrl:NavController) { }

    ngOnInit(){
    }



    async homepage(){
      const {username, password} = this
      let test : boolean = true;
      
      try{
          const res=await this.afAuth.auth.signInWithEmailAndPassword(username + '@gmail.com', password )
      }

      catch(err)
      {
        console.dir(err)
        test = false;

        if(test == false){
          console.log("User not found");
          const alertController = document.querySelector('ion-alert-controller');
          await alertController.componentOnReady();

          const alert = await alertController.create({
            header: 'Error',
            subHeader: 'User not found',
            message: 'Please register first',
            buttons: ['OK']
          });
          return await alert.present();
        } 
      }

      if (test==true)
      {
        console.log("user found")
        this.navCtrl.navigateForward('/home')
      }
      this.username="";
      this.password="";
    }
}

