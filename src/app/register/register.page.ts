import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  username: string = ""
  password: string = ""
  cpassword: string = ""

  constructor(public navCtrl:NavController, public afAuth: AngularFireAuth) {}
  
  ngOnInit() {
  }
  
  async register(){
    const {username, password, cpassword} = this
    if (password != cpassword){
      return console.error("passwords don't match")
    }
    
    try{
      const res = await this.afAuth.auth.createUserWithEmailAndPassword (username + '@gmail.com', password)
      console.log(res)
      this.navCtrl.navigateForward('/login')
    }
    catch(error){
      console.dir(error)  
    }
  }
}

