import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTablePage } from './new-table.page';

describe('NewTablePage', () => {
  let component: NewTablePage;
  let fixture: ComponentFixture<NewTablePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewTablePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
